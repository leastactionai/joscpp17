# JOSCPP17 

* Source code from [C++17 - The Complete Guide](http://cppstd17.com)
* Plan is to build explanations around the source code, obviously without reading the book, and 
   publish these explanations at [Read The Docs](https://readthedocs.org)
